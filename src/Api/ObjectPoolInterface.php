<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\ObjectPool\Api;

/**
 * Interface ObjectPoolInterface
 * @api
 */
interface ObjectPoolInterface extends \ArrayAccess
{
    /**
     * Get all objects in pool
     * @return \object[]
     */
    public function getAll(): array;

    /**
     * Is an object in pool?
     * @param string $name
     * @return bool
     */
    public function has(string $name): bool;

    /**
     * Get an object in pool
     * @param string $name
     * @return object|null
     */
    public function get(string $name): ?object;
}
